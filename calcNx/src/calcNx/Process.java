package calcNx;

import java.util.Scanner;

/**
 * @author devnix
 *
 */
public class Process {

	/**
	 * @param args TODO: Descripci�n de la clase calcNx M�todo principal de
	 *             procesamiento para las siguientes operaciones: - Suma - Resta -
	 *             Multiplicaci�n - Divisi�n - Ra�z
	 * 
	 *             Compiladores | Uniremington
	 */
	private static final String[] OPERATIONS = { "Suma", "Resta", "Multiplicaci�n", "Divisi�n", "Ra�z" };
	private static String value1, value2;
	private static Scanner sc;

	public static void main(String[] args) {
		print("Inicio del proceso....");
		for (int i = 0; i < OPERATIONS.length; i++) {
			print("Operaci�n a realizar: " + OPERATIONS[i]);
			setValue1(getScValue());
			setValue2(getScValue());
			value1 = i == 0 ? String.valueOf((Integer.parseInt(getValue1()) + Integer.parseInt(getValue2())))
					: i == 1 ? String.valueOf((Integer.parseInt(getValue1()) - Integer.parseInt(getValue2())))
							: i == 2 ? String.valueOf((Integer.parseInt(getValue1()) * Integer.parseInt(getValue2())))
									: i == 3 ? String
											.valueOf((Float.parseFloat(getValue1()) / Float.parseFloat(getValue2())))
											: String.valueOf((float) Math.pow(Integer.parseInt(getValue1()),
													1 / Integer.parseInt(getValue2())));
			print("Resultado de la " + OPERATIONS[i] + " = " + getValue1());
		}
		print("Fin del proceso....");
	}

	public static String getScValue() {
		sc = new Scanner(System.in);
		print("Ingrese un n�mero");
		return sc.next();
	}

	/**
	 * 
	 * @param message
	 * 
	 *                FIXME: En caso de implementar log4j remplazar:
	 *                System.out.println(message + "\n");
	 */

	public static void print(String message) {
		System.out.println(message + "\n");
	}

	/**
	 * @return the value1
	 */
	public static String getValue1() {
		return value1;
	}

	/**
	 * @param value1 the value1 to set
	 */
	public static void setValue1(String value1) {
		Process.value1 = value1;
	}

	/**
	 * @return the value2
	 */
	public static String getValue2() {
		return value2;
	}

	/**
	 * @param value2 the value2 to set
	 */
	public static void setValue2(String value2) {
		Process.value2 = value2;
	}

}
